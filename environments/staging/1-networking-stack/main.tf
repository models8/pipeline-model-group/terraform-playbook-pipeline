locals {
  name_suffix = "${var.project}-${var.environment}"
  required_tags = {
    project = var.project
    environment = var.environment
  }
  tags = merge(var.resources_tags, local.required_tags)
}

provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-5hr6tshr6t8h4"
    region = "us-east-2"
    encrypt = true
  }
}

module "vpc" {
  source = "../../../modules/vpc"
  resources_name_suffix = local.name_suffix
  vpc_cidr_block = var.vpc_cidr_block
  resources_tags = local.tags
}

module "sg_elb" {
  source = "../../../modules/security_group"
  sg_name = "elb-public-${local.name_suffix}"
  sg_description = "Public security group for elactic load balancer"
  vpc_id = module.vpc.vpc_id
  ingress_rules_cidr = [{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ssh_allowed_ip]
  },{
    from_port = 80
    to_port = 80
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }]
  resources_tags = local.tags
}

module "sg_ec2" {
  source = "../../../modules/security_group"
  vpc_id = module.vpc.vpc_id
  sg_name = "ec2-restricted-${local.name_suffix}"
  sg_description = "Restricted security group for ec2 instances"
  ingress_rules_cidr = [{
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  },{
    #Allow ssh connection only for staging env
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ssh_allowed_ip]
  },{
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [var.vpc_cidr_block]
  }]
  resources_tags = local.tags
}


