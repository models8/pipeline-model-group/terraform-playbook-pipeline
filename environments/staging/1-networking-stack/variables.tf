
variable "project" {
  type = string
  description = "project name"
}

variable "environment" {
  type = string
  description = "environment-name"
}

variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
  default = "10.1.0.0/16"
}

variable "ssh_allowed_ip" {
  type = string
  description = "my public ip address for allow ssh connexions"
  default = "0.0.0.1/32"
  sensitive = true
}

variable "resources_tags" {
  type = map(string)
  description = "resources default tags"
  default = {}
}

variable "event_emitter" {
  type = string
  description = "identity of build trigger"
}
