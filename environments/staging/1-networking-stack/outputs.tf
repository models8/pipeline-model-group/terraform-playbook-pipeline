output "vpc_id" {
  value = module.vpc.vpc_id
  description = "Main vcp id"
}

output "public_subnet_ids" {
  value = module.vpc.public_subnet_ids
  description = "list of subnet ids"
}

output "elb_security_group_id" {
  value =  module.sg_elb.sg_id
  description = "security group for load balancers"
}

output "ec2_security_group_id" {
  value = module.sg_ec2.sg_id
  description = "security group for ec2 instances"
}
