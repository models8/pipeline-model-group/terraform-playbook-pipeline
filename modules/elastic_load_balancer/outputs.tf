output "api_target_group_arn" {
  value = aws_alb_target_group.api_group.arn
  description = "api target group arn"
}

output "front_target_group_arn" {
  value = aws_alb_target_group.front_group.arn
  description = "front target group arn"
}

output "elb_url" {
  value = aws_alb.alb.dns_name
  description = "elb dns name"
}
