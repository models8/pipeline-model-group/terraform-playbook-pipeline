terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


resource "aws_alb" "alb" {
  name = "alb-${var.ressource_name_suffix}"
  load_balancer_type = "application"
  ip_address_type = "ipv4"
  internal = false
  security_groups = var.elb_security_groups
  subnets = var.elb_subnets
  tags = var.resources_tags
}


resource "aws_alb_target_group" "api_group" {
  name = "tg-api-${var.ressource_name_suffix}"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_api"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/${var.api_name}/test"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }
  tags = var.resources_tags
}

resource "aws_alb_target_group" "front_group" {
  name = "tg-front-${var.ressource_name_suffix}"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_front"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }
  tags = var.resources_tags
}


resource "aws_alb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.front_group.arn
  }

}

resource "aws_alb_listener_rule" "listener_rules" {
  listener_arn = aws_alb_listener.alb_listener.arn
  priority = 1
  condition {
    path_pattern {
      values = ["/${var.api_name}/*"]
    }
  }
  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.api_group.arn

    forward {
      target_group {
        arn = aws_alb_target_group.api_group.arn
        weight = 100
      }
      /*--------beug terraform Solution de contournement :---------
      Teraform veut minimum 2 targets groups ... Donc je met 2 target-groups identiques
      sinon terraform refuse de compiler.
      */
      target_group {
        arn = aws_alb_target_group.api_group.arn
      }
      /*---------------------------------------------------------*/
      stickiness {
        duration = 3600
        enabled = true
      }
    }
  }
}
