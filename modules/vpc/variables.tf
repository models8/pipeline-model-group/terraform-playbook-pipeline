variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
}

variable "resources_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
}

/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}



