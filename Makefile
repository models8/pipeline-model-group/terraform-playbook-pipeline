ifeq (init,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (plan,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (apply,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (destroy,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (build-staging,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (build-prod,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


IMAGE_NAME=enzo13/terraform-pipeline


#--------------------TERRAFORM --------------------------


EVENT_EMITTER=computer

BACKEND_STAGING_NETWORK=terraform/$(EVENT_EMITTER)/staging/networking/terraform.tfstate
VARS_STAGING_NETWORK = -var="event_emitter=computer"  -var="environment=staging"

BACKEND_STAGING_ECS=terraform/$(EVENT_EMITTER)/staging/ecs/terraform.tfstate
VARS_STAGING_ECS = -var="event_emitter=computer"  -var="environment=staging" -var="s3_remote_state_path=$(BACKEND_STAGING_NETWORK)"

BACKEND_PROD=terraform/$(EVENT_EMITTER)/production/global/terraform.tfstate
VARS_PROD = -var="event_emitter=computer"  -var="environment=production"

#-var="s3_remote_state_path=production"


cmp: #commit merge and push all
	git add --all && git commit -m"edit"
	git checkout production && git merge master
	git push --all
	git checkout master






.PHONY: init
init:
	if [ $(RUN_ARGS) = "prod" ];																																	\
	then 																																							\
	  	terraform -chdir="./environments/$(RUN_ARGS)" init -backend-config="key=$(BACKEND_PROD)" ;																											\
	else																																							\
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" init -backend-config="key=$(BACKEND_STAGING_NETWORK)" ;																						\
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" init -backend-config="key=$(BACKEND_STAGING_ECS)";		\
	fi


.PHONY: plan
plan:
	if [ $(RUN_ARGS) = "prod" ];																				\
	then 																										\
	  terraform -chdir="./environments/$(RUN_ARGS)" plan $(VARS_PROD);										\
	else																										\
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" plan $(VARS_STAGING_NETWORK);						\
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" plan $(VARS_STAGING_ECS)		;			\
	fi


.PHONY: apply
apply:
	if [ $(RUN_ARGS) = "prod" ];																				\
	then 																										\
	  terraform -chdir="./environments/$(RUN_ARGS)" apply -auto-approve $(VARS_PROD);										\
	else																										\
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" apply -auto-approve $(VARS_STAGING_NETWORK);						\
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" apply -auto-approve  $(VARS_STAGING_ECS);	\
	fi



.PHONY: destroy
destroy:
	if [ $(RUN_ARGS) = "prod" ];																										\
	then 																																\
	  terraform -chdir="./environments/$(RUN_ARGS)" destroy -auto-approve $(VARS_PROD);																\
	else																																\
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" destroy -auto-approve $(VARS_STAGING_ECS);			\
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" destroy -auto-approve $(VARS_STAGING_NETWORK);	 										\
	fi


#--------------------DOCKERFILE --------------------------
.PHONY: build-staging
build-staging:
	docker build --rm --build-arg ENVIRONMENT=staging -t $(IMAGE_NAME)-staging:latest -t $(IMAGE_NAME)-staging:$(RUN_ARGS) ./
	docker push --all-tags $(IMAGE_NAME)-staging

.PHONY: build-prod
build-prod:
	docker build --rm --build-arg ENVIRONMENT=prod -t $(IMAGE_NAME)-prod:latest -t $(IMAGE_NAME)-prod:$(RUN_ARGS) ./
	docker push --all-tags $(IMAGE_NAME)-prod
