FROM hashicorp/terraform:1.1.9 AS build
WORKDIR /terraform
ARG ENVIRONMENT
RUN test -n "$ENVIRONMENT"
COPY ./environments/$ENVIRONMENT ./environments/$ENVIRONMENT
COPY ./modules  ./modules

FROM hashicorp/terraform:1.1.9
ARG ENVIRONMENT
RUN apk add --no-cache curl jq
COPY --from=build /terraform /terraform
WORKDIR /terraform/environments/$ENVIRONMENT
ENTRYPOINT [""]


